 # Codeigniter 3.1.9 + Blade Templating Engine + Bootstrap 3

This library used Laravel`s blade templating engine.

### Installation
 1. Clone/Download this repository
 2. Copy all files to your Project folder
 3. Open http://localhost/[project-folder]/
 
 ### Usage

  ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>{{$page_title}} - CodeIgniter</title>	
    </head>
    <body>
    <h1>Welcome {{$person_name}}!</h1>
    </body>
    </html>
